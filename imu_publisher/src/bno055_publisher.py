#!/usr/bin/env python
import json
import time
from Adafruit_BNO055 import BNO055
from sensor_msgs.msg import Imu
from sensor_msgs.msg import Temperature
import rospy
import tf
#How often to update the BNO sensor data (in hertz).
#
#BNO_UPDATE_FREQUENCY_HZ = 10

def load_params():
    try:
        imu_port = rospy.get_param('bno_imu/serial_addr','/dev/ttyAMA0' )
        imu_pin = rospy.get_param('bno_imu/imu_pin', 18)
        #Name of the file to store calibration data. using server.py example
        #needs update eventually.  Calibration data is stored in JSON format.
        calib_file_path = rospy.get_param('bno_imu/calib_path','/home/ubuntu/catkin_ws/src/sensor_array/imu_publisher/src/calibration.json')

    except KeyError:
        rospy.logerr('Params not loaded')

    return imu_port, imu_pin, calib_file_path
def main():
    # Imu message to be populated:
    imu_msg = Imu()
    # Temperature message to be populated
    temp_msg = Temperature()
    rospy.loginfo('Starting BNO055 IMU Driver')
    imu_port, imu_pin, calib_file_path = load_params()
    rospy.init_node('imu_node')
    imu_pub = rospy.Publisher('Imu/imutopic', Imu, queue_size=100)
    temp_pub = rospy.Publisher('/temperature/box',Temperature, queue_size=100)
    rate = rospy.Rate(10)

    # ROS tf broadcaster
    imu_tf = tf.TransformBroadcaster()

    try:
        bno = BNO055.BNO055(serial_port=imu_port, rst=imu_pin)
    except RuntimeError:
        rospy.logerr('Could not detect device. Is the serial port correct?')
        exit()
    
    #Initlaize bno
    if not bno.begin():
        rospy.logfatal('Failed to initialize BNO055 IMU. Please check if sensor is connected')
        



    #do intial sensor error check
    status, self_test, error = bno.get_system_status()
    rospy.loginfo('System status: {0}'.format(status))
    rospy.loginfo('Self test result (0x0F is normal): 0x{0:02X}'.format(self_test))
    if status == 0x01:
        rospy.loginfo('System error: {0}'.format(error))
        rospy.loginfo('See datasheet section 4.3.59 for the meaning.')
        exit()

    #turn off the magnetometer
    #see datasheet table 3-5
    #
    imu_mode = 0X08
    bno.set_mode(imu_mode)

    #Load calibration from disk
    #uses server.py example for calibration, this should be updated
    #
    with open(calib_file_path, 'r') as cal_file:
        data = json.load(cal_file)

    bno.set_calibration(data)

    #Begin reading data if error stuff went okay
    
    while not rospy.is_shutdown():

        # temperature in C for diagnostic
        temp = bno.read_temp()
        temp_msg.header.stamp = rospy.Time.now()
        temp_msg.header.frame_id = 'imu_link'


        # Grab Quaternion
        x, y, z, w = bno.read_quaternion()
        roll, pitch, yaw = bno.read_euler()
        #Return the current linear acceleration (acceleration from movement,
        #not from gravity) reading as a tuple of X, Y, Z values in meters/second^2
        x_lin, y_lin, z_lin = bno.read_linear_acceleration()

        #Return the current accelerometer reading as a tuple of X, Y, Z values
        #in meters/second^2.
        x_ang, y_ang, z_ang = bno.read_gyroscope()

        #get cal status and any errors
	    #
        # sys, gyro, accel, mag = bno.get_calibration_status()
        # status, self_test, error = bno.get_system_status(run_self_test=False)
        #if error != 0:
	    #    print 'Error! Value: {0}'.format(error)
        #    print('x={0:0.2F} y={1:0.2F} z={2:0.2F} w={3:0.2F}\tSys_cal={4} Gyro_cal={5} Accel_cal={6} Mag_cal={7}'.format(x, y, z, w, sys, gyro, accel, mag))
        #    print('x_L={0:0.2F} y_L={1:0.2F} z_L={2:0.2F}\tx_A={3:0.2F} y_A={4:0.2F} z_A={5:0.2F}'.format(x_lin, y_lin, z_lin, x_ang, y_ang, z_ang))

        imu_msg.orientation.x = x
        imu_msg.orientation.y = y
        imu_msg.orientation.z = z
        imu_msg.orientation.w = w

        imu_msg.linear_acceleration.x = x_lin
        imu_msg.linear_acceleration.y = y_lin
        imu_msg.linear_acceleration.z = z_lin

        imu_msg.angular_velocity.x = x_ang
        imu_msg.angular_velocity.y = y_ang
        imu_msg.angular_velocity.z = z_ang

	translate_x = 0
	translate_y = 0.254
        translate_z = 0
        imu_orientation = quaternion = tf.transformations.quaternion_from_euler(0, 1.571, 0)
        imu_tf.sendTransform(
            (translate_x, translate_y, translate_z),
            imu_orientation,
            rospy.Time.now(),
            'imu_link',
            'base_link')
        
        #Publish message with timestamp
        imu_msg.header.stamp = rospy.Time.now()
        # ROS Localizatoin node (ekf_node) should be able to use tf broadcaster's imu_link to base_link message to transform this data
        imu_msg.header.frame_id = 'imu_link'
        rospy.loginfo(imu_msg)
        imu_pub.publish(imu_msg)
        rate.sleep()


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
