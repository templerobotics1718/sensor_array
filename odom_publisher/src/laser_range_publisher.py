#!/usr/bin/env python
import rospy
from nav_msgs.msg import Odometry
from std_msgs.msg import Int8

pub = rospy.Publisher('/odom', Odometry, queue_size=50)
def confirmation_chicken(confirmation):
    if(confirmation == 1):
        rospy.Subscriber('/odom', Odometry, publishing_to_odom)
    else:
        print("Laser Range unable to detect Aruco Marker")

def publishing_to_odom(Odometry):
    pub.publish(Odometry)

def pubsub():
    rospy.init_node('laser_range_publisher', anonymous=True)
    rospy.Subscriber('/laser_range_confirmation', Int8, confirmation_chicken)
    rospy.spin()

if __name__ == '__main__':
    try:
        pubsub()
    except rospy.ROSInterruptException:
        pass
