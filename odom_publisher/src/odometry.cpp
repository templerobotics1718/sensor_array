#include <ros/ros.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Int32MultiArray.h>

//initialization of variables
float x, y, theta;
double lwheel_linear_vel, rwheel_linear_vel;
double lwheel_angular_vel, rwheel_angular_vel;
double wheel_radius, wheel_base;

//Beginning of callback

//Robot's position
void pose_sub_callback(const nav_msgs::OdometryConstPtr& msg) {		

	x = msg->pose.pose.position.x;
	y = msg->pose.pose.position.y;
	theta = msg->pose.pose.position.z;
}

//angular vel

void wheel_angular_vel_enc_sub_callback(const std_msgs::Int32MultiArrayConstPtr& array)
{
	// array of int to int*******************************************
	//method 1
	 int   f_rwheel_angular_vel = array->data[0];			
	 int	b_rwheel_angular_vel = array->data[1];
	 int   f_lwheel_angular_vel = array->data[2];
	 int	b_lwheel_angular_vel = array->data[3];

	
	lwheel_angular_vel = ((f_lwheel_angular_vel + b_lwheel_angular_vel) / 2);
	rwheel_angular_vel = ((f_rwheel_angular_vel + b_rwheel_angular_vel) / 2);
}

//End of callback

int main(int argc, char** argv) {
	ros::init(argc, argv, "odometry_publisher");

	ros::NodeHandle n;
	ros::Publisher odom_pub = n.advertise<nav_msgs::Odometry>("odom", 1000);    //edited
	tf::TransformBroadcaster odom_broadcaster;

	ros::Time current_time, last_time;
	current_time = ros::Time::now();
	last_time = ros::Time::now();



		//getting parameter data
	if (n.getParam("wheel_radius_param", wheel_radius))
	{

	}
	else
	{
		wheel_radius = 0.021; // default value
	}

	if (n.getParam("wheel_base_param", wheel_base))
	{

	}
	else
	{
		wheel_base = 0.15716; // default value
	}


	//rate at which we'll publish odometry information
	ros::Rate r(40.0);		//subject to change

	//subscriptions
	ros::Subscriber wheel_angular_vel_enc_sub = n.subscribe("wheel_angular_vel_enc", 50, wheel_angular_vel_enc_sub_callback); ///***
	ros::Subscriber pose_sub = n.subscribe("odom_filtered", 50, pose_sub_callback);

	while (ros::ok()) {


		//compute linear_velocity given the velocities of the robot
		double linear_vel = ((((wheel_radius) / 2)*(lwheel_angular_vel))
			+ (((wheel_radius) / 2)*(rwheel_angular_vel)));

		//compute angular velocity given the velocities of the robot
		double angular_vel = (((-(wheel_radius) / wheel_base)*(lwheel_angular_vel))			//remember to fetch wheel_base from param - done
			+ (((wheel_radius) / wheel_base)*(rwheel_angular_vel)));

		//computes odometry
		double dt = (current_time - last_time).toSec();    ///
		double delta_x = (linear_vel * cos(theta)) * dt;
		double delta_y = (linear_vel * sin(theta)) * dt;
		double delta_th = theta * dt;

		//updates odometry info.
		double x = x + delta_x;
		double y = y + delta_y;
		double theta = theta + delta_th;

		//quaternion created from yaw
		geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(theta);

		//publish the transform over tf
		geometry_msgs::TransformStamped odom_trans;
		odom_trans.header.stamp = current_time;
		odom_trans.header.frame_id = "odom";
		odom_trans.child_frame_id = "base_link";

		odom_trans.transform.translation.x = x;
		odom_trans.transform.translation.y = y;
		odom_trans.transform.translation.z = theta;
		odom_trans.transform.rotation = odom_quat;


		//publish the odometry message over ROS
		nav_msgs::Odometry odom;
		odom.header.stamp = current_time;
		odom.header.frame_id = "odom";

		//set the position
		odom.pose.pose.position.x = x;
		odom.pose.pose.position.y = y;
		odom.pose.pose.position.z = theta;
		odom.pose.pose.orientation = odom_quat;

		//set the velocity 
		odom.child_frame_id = "base_link";
		odom.twist.twist.linear.x = linear_vel;
		odom.twist.twist.linear.y = 0;			// linear velocity for y is 0 because our robot doesn't maneuver sideways.	
		odom.twist.twist.angular.z = angular_vel;

		//publish odom and odom_trans
		odom_broadcaster.sendTransform(odom_trans);
		odom_pub.publish(odom);
		last_time = current_time;

		ros::spinOnce();
		r.sleep();

	}

}